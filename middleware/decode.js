module.exports = function () {

    function decode (req, res, next) {
        
        const data = Buffer.from(req.body.sketch.data, 'base64').toString('ascii')
        req.body.sketch.data = data
        
        return next()
    }


    return decode;
}