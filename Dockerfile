FROM node

# arduino-cli
RUN cp bin/arduino-cli /bin
RUN arduino-cli config init
RUN arduino-cli core update-index
RUN arduino-cli core install arduino:avr

# app
RUN mkdir -p /usr/src/scratch-server-app
WORKDIR /usr/src/scratch-server-app

COPY package*.json /usr/src/scratch-server-app
RUN cd /usr/src/scratch-server-app
RUN npm install

COPY . /usr/src/scratch-server-app

EXPOSE 8081

CMD npm start