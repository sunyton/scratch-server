const restify = require('restify')
const decode = require('./middleware/decode')
const execa = require('execa')
const fs = require('fs')
const path = require('path')

const server = restify.createServer({
    name: 'test',
    version: '0.1'
})

server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser())
server.use(restify.plugins.bodyParser())
server.use(decode())


server.post('/compile', async (req, res, next) => {
    if (req.body.sketch.data) {
        const data = req.body.sketch.data
        const name = req.body.sketch.name
        // data 唯一
        const {stderr: stderr1} = await execa('arduino-cli', ['sketch', 'new', './tmp/'+name])
        const {stderr} = await execa('echo', [data, '>>', './tmp/'+name+'/'+name+'.ino'])
        if (!stderr) {
            const {stderr} = await execa('arduino-cli', ['compile', '--fqbn', 'arduino:avr:uno', './tmp/'+name])
            if (!stderr) {
                const hex = fs.readFileSync(path.resolve(__dirname, './tmp/'+name+'/'+name+'.arduino.avr.uno.hex'))
                execa('rm', ['-r', './tmp/'+name])
                res.send({hex: Buffer.from(hex).toString('base64')})
            } else {
                res.send('compile failed')
            }
            
        } else {
            res.send('sketch write failed')
        }
        
    } else {

        res.send('no sketch compile..')
    }
})

server.listen(8081, () => {
    console.log("server is running...")
})